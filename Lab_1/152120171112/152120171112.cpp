#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

double addition(vector<double> vect);
double product(vector<double> vect);
double average(vector<double> vect);
double the_smallest(vector<double> vect);

int main(){

vector<double> number_list;
int number;

ifstream input_file("input.txt");

if (input_file.is_open()){      // to open file
    string dummyLine;
    getline(input_file, dummyLine);

    while (input_file){     // to read first line
        while(input_file >> number){ //
            number_list.insert(number_list.end(),number);
        }
           
    }
    cout << "Sum is "<< addition(number_list) << endl;
    cout << "Product is " << product(number_list) << endl;
    cout << "Average is " <<average(number_list) <<endl;
    cout << "The smallest number is " << the_smallest(number_list) << endl;
}   
else{
    cout <<"The file did not open.";
}

system ("pause");
return 0;
}

double addition(vector<double> vect){
int sum=0;

    for(int i=0;i<vect.size();i++){

        sum += vect[i];
    }
    
    return sum;
}

double product(vector<double> vect){
int product=1;

    for(int i=0;i<vect.size();i++){

        product *= vect[i];
    }
    return product;
}

double average(vector<double> vect){

double average = addition(vect)/vect.size();
return average;
}

double the_smallest(vector<double> vect){
int min = vect[0];     //consider as the first integer is the smallest of the values

for(int i=0;i<vect.size();i++){

    if(min>vect[i]){
        min = vect[i];
    }
} 
    return min;
}