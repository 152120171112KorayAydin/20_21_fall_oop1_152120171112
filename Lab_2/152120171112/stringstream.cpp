#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
	// Complete this function
    vector<int> v1;
    stringstream ss(str);
    char ch;
    int a;
    
    ss >> a;
    v1.push_back(a);
    
    for(int i=0;i<str.size();i++){
        
        ss >> ch >> a;
        
        if(ss.fail()){
            break;
        }
        else{
            v1.push_back(a);
        }
        
    }return v1;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}