#include <iostream>
#include <string>
using namespace std;

int main() {
	// Complete the program
    string str1,str2,temp;
    cin >> str1 >> str2;
    
    cout << str1.size() << " " << str2.size() << endl;
    cout << str1 + str2 << endl;
    
    temp = str2;
    
    str2[0] = str1[0];
    str1[0] = temp[0];
    
    cout << str1 << " " << str2 << endl;
    
    
    return 0;
}