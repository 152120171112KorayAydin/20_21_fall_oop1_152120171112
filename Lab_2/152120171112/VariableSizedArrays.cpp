#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    
    int row,queries,number_value;
    
    cin >> row >> queries;
    
    int *array[row];  
    
    for(int i=0;i<row;i++){
        
        cin >> number_value;
        
        array[i] = new int[number_value];
        
        for(int j=0;j<number_value;j++){
            cin  >> array[i][j];
        }
    }
    for(int i=0;i<queries;i++){
        
        int val1,val2;
        
        cin >> val1 >> val2;
        
        cout << array[val1][val2] << endl;
        
    }
    return 0;
}